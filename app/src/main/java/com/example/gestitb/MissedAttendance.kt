package com.example.gestitb

data class MissedAttendance(val id: Int, var name: String, var module: String, var date: String, var justified: Boolean)
