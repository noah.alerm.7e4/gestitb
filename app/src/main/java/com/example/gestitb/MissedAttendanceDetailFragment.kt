package com.example.gestitb

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout

class MissedAttendanceDetailFragment : Fragment(R.layout.fragment_missed_attendance_detail) {
    //ATTRIBUTES
    //layout
    private lateinit var nameLayout: TextInputLayout
    private lateinit var nameText: EditText
    private lateinit var moduleSpinner: Spinner
    private lateinit var dateButton: Button
    private lateinit var justificationCheckBox: CheckBox
    private lateinit var addButton: Button

    //fragment
    private lateinit var adapter: ArrayAdapter<String>

    //view model
    private val viewModel: MissedAttendanceListViewModel by activityViewModels()

    //METHODS
    /**
     * This method is used to update the data in case it's showing an already created item.
     */
    private fun updateUI() {
        nameText.setText(arguments?.getString("name"))
        moduleSpinner.setSelection(adapter.getPosition(arguments?.getString("module")))
        dateButton.text = arguments?.getString("date")

        if (arguments?.getBoolean("justified")!!)
            justificationCheckBox.isChecked = true
    }

    //ON VIEW CREATED
    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //IDs
        nameLayout = view.findViewById(R.id.name_text)
        nameText = view.findViewById(R.id.name_edit_text) as EditText
        moduleSpinner = view.findViewById(R.id.class_spinner)
        dateButton = view.findViewById(R.id.date_button)
        justificationCheckBox = view.findViewById(R.id.justified_checkbox)
        addButton = view.findViewById(R.id.add_button)

        //ADD BUTTON TEXT
        if (arguments?.getBoolean("add_item")!!)
            addButton.text = "ADD"

        //SPINNER SETUP
        adapter = ArrayAdapter(requireContext(), R.layout.support_simple_spinner_dropdown_item,
            resources.getStringArray(R.array.modules))

        moduleSpinner.adapter = adapter

        //UPDATE UI
        updateUI()

        //NAME TEXT
        nameText.isCursorVisible = false

        nameText.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                //CLOSE KEYBOARD ON PRESSED ENTER
                if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    val imm = context?.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(nameText.windowToken, 0)

                    nameText.clearFocus()
                    nameText.isCursorVisible = false

                    return true
                }
                return false
            }
        })

        //ON CLICK
        //nameText
        nameText.setOnClickListener {
            nameLayout.error = null
            nameText.isCursorVisible = true
        }

        //date button
        dateButton.setOnClickListener {
            //DATE PICKER
            val datePicker = DatePickerDialog(requireContext())
            datePicker.setOnDateSetListener { _, year, month, dayOfMonth ->
                val timePicker = TimePickerDialog(requireContext(),
                    { _, hourOfDay, minute ->
                        dateButton.text = "$hourOfDay:$minute $dayOfMonth/${month + 1}/$year"
                    }, 0, 0, true)
                timePicker.show()
            }
            datePicker.show()
        }

        //add button
        addButton.setOnClickListener {
            //NO NAME
            if (nameText.text.toString() == "") {
                nameLayout.error = "Student's name is required"
            }
            //NO DATE
            else if (dateButton.text.toString() == getString(R.string.default_date_text)) {
                val snackbar = Snackbar.make(view, "You must set a date", Snackbar.LENGTH_SHORT)
                snackbar.setBackgroundTint(ContextCompat.getColor(requireContext(), R.color.dark_red))
                snackbar.show()
            }
            //ALL FIELDS FILLED
            else{
                //CREATION DIALOG
                val builder = AlertDialog.Builder(requireContext())
                builder.setTitle("Missed attendance created")
                //text
                if (justificationCheckBox.isChecked)
                    builder.setMessage("The student ${nameText.text} has missed ${moduleSpinner.selectedItem}" +
                            " on ${dateButton.text} with justification")
                else
                    builder.setMessage("The student ${nameText.text} has missed ${moduleSpinner.selectedItem}" +
                            " on ${dateButton.text} without justification")
                //reset all fields on OK pressed
                builder.setPositiveButton("OK") { _: DialogInterface, _: Int ->
                    nameText.text = null
                    moduleSpinner.setSelection(0)
                    dateButton.text = getString(R.string.default_date_text)
                    justificationCheckBox.isChecked = false
                }
                builder.setCancelable(false)
                builder.create().show()

                //LIST UPDATE
                //NEW MISSED ATTENDANCE
                if (arguments?.getBoolean("add_item")!!) {
                    viewModel.getMissedAttendances().add(MissedAttendance(viewModel.getMissedAttendances().size,
                        nameText.text.toString(), moduleSpinner.selectedItem.toString(), dateButton.text.toString(),
                        justificationCheckBox.isChecked))
                }
                //UPDATED MISSED ATTENDANCE
                else {
                    for (item in viewModel.getMissedAttendances()) {
                        if (item.id == arguments?.getInt("id")!!) {
                            item.name = nameText.text.toString()
                            item.module = moduleSpinner.selectedItem.toString()
                            item.date = dateButton.text.toString()
                            item.justified = justificationCheckBox.isChecked
                        }
                    }
                }

                findNavController().navigate(R.id.action_Detail_to_List)
            }
        }
    }
}
