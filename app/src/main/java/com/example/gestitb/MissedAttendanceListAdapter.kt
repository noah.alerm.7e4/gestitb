package com.example.gestitb

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView

class MissedAttendanceListAdapter(private val missedAttendances: List<MissedAttendance>) :
    RecyclerView.Adapter<MissedAttendanceListAdapter.MissedAttendanceListViewHolder>() {

    //METHODS
    /**
     * This method is used to create a View Holder and to set up it's view.
     * @param parent Adapter's parent (used to get context)
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MissedAttendanceListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.missed_attendance_list_item,
            parent, false)

        return MissedAttendanceListViewHolder(view)
    }

    /**
     * This method is used to set up the data of each item.
     * @param holder View Holder
     * @param position Current item
     */
    override fun onBindViewHolder(holder: MissedAttendanceListViewHolder, position: Int) {
        holder.bindData(missedAttendances[position])

        //ITEM ON CLICK
        holder.itemView.setOnClickListener {
            //CURRENT ITEM
            val item = missedAttendances[position]

            //NAVIGATION
            val action = MissedAttendanceListFragmentDirections.actionListToDetail(item.id, item.name,
                item.module, item.date, item.justified, false)
            Navigation.findNavController(holder.itemView).navigate(action)
        }
    }

    /**
     * This method is used to get the total amount of items in the Recycler View.
     */
    override fun getItemCount(): Int {
        return missedAttendances.size
    }


    //VIEW HOLDER
    class MissedAttendanceListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //ATTRIBUTES
        private var name: TextView = itemView.findViewById(R.id.item_name)
        private var module: TextView = itemView.findViewById(R.id.module)
        private var date: TextView = itemView.findViewById(R.id.date)
        private var justificationCheck: ImageView = itemView.findViewById(R.id.justification_check)

        //METHODS
        /**
         * This method is used to set up the data of each item of the missed attendances list.
         */
        @SuppressLint("SetTextI18n")
        fun bindData(missedAttendance: MissedAttendance) {
            name.text = missedAttendance.name

            //This conditional controls the length of the module, as some of them are too long.
            if (missedAttendance.module.length >= 35)
                module.text = missedAttendance.module.substring(0, 35)+"..."
            else
                module.text = missedAttendance.module

            date.text = missedAttendance.date

            if (missedAttendance.justified)
                justificationCheck.setImageResource(R.drawable.check)
            else
                justificationCheck.setImageDrawable(null)
        }
    }
}
