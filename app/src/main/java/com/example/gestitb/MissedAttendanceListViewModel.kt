package com.example.gestitb

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import java.text.SimpleDateFormat
import java.util.*


@SuppressLint("SimpleDateFormat")
class MissedAttendanceListViewModel(application: Application) : AndroidViewModel(application) {
    //ATTRIBUTES
    private var missedAttendances = mutableListOf<MissedAttendance>()
    //date
    private var calendar: Calendar = Calendar.getInstance()
    private var dateFormat: SimpleDateFormat = SimpleDateFormat("HH:mm MM/dd/yyyy")
    private var date: String = dateFormat.format(calendar.time)

    //INIT
    init {
        for (i in 1..100) {
            missedAttendances.add(MissedAttendance(i, "Student #$i",
                application.resources.getStringArray(R.array.modules).random(), date, i%2==0))
        }
    }

    //GETTER
    fun getMissedAttendances() = missedAttendances
}
