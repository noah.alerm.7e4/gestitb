package com.example.gestitb

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper.ACTION_STATE_SWIPE
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator

import com.google.android.material.snackbar.Snackbar

class MissedAttendanceListFragment : Fragment(R.layout.fragment_missed_attendance_list) {
    //ATTRIBUTES
    private lateinit var recyclerView: RecyclerView
    private lateinit var additionButton: FloatingActionButton

    private val viewModel: MissedAttendanceListViewModel by activityViewModels()

    //ON VIEW CREATED
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //IDs
        recyclerView = view.findViewById(R.id.recycler_view)
        additionButton = view.findViewById(R.id.add_item_button)

        //RECYCLER VIEW
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = MissedAttendanceListAdapter(viewModel.getMissedAttendances())

        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            //ATTRIBUTES
            private var swipeBack = false

            //MOVE
            override fun onMove(v: RecyclerView, h: RecyclerView.ViewHolder, t: RecyclerView.ViewHolder) = false

            //SWIPE
            override fun onSwiped(h: RecyclerView.ViewHolder, dir: Int) {
                val position = h.bindingAdapterPosition
                val item = viewModel.getMissedAttendances()[position]

                viewModel.getMissedAttendances().removeAt(position)
                (recyclerView.adapter as MissedAttendanceListAdapter).notifyItemRemoved(position)

                //SNACKBAR (UNDO)
                val snackbar = Snackbar.make(recyclerView, "${item.name} was removed from the list", Snackbar.LENGTH_LONG)
                snackbar.setAction("UNDO") {
                    viewModel.getMissedAttendances().add(position, item)
                    (recyclerView.adapter as MissedAttendanceListAdapter).notifyItemInserted(position)
                    recyclerView.scrollToPosition(position)
                }
                snackbar.setActionTextColor(ContextCompat.getColor(requireContext(), R.color.teal_200))
                snackbar.show()
            }

            //CHILD DRAW
            override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                                     dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

                //SWIPE BACK
                if (actionState == ACTION_STATE_SWIPE) {
                    setTouchListener(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                }

                //SWIPE DECORATOR (see Gradle Implementation)
                RecyclerViewSwipeDecorator.Builder(requireContext(), c, recyclerView, viewHolder, dX,
                dY, actionState, isCurrentlyActive)
                    .addBackgroundColor(ContextCompat.getColor(requireContext(), R.color.red))
                    .addActionIcon(R.drawable.trash_can)
                    .create().decorate()
            }

            //SWIPE BACK
            override fun convertToAbsoluteDirection(flags: Int, layoutDirection: Int): Int {
                if (swipeBack) {
                    swipeBack = false
                    return 0
                }
                return super.convertToAbsoluteDirection(flags, layoutDirection)
            }

            @SuppressLint("ClickableViewAccessibility")
            private fun setTouchListener(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                recyclerView.setOnTouchListener { _, event ->
                    swipeBack = event.action == MotionEvent.ACTION_CANCEL || event.action == MotionEvent.ACTION_UP

                    if (dX <= -600F)
                        swipeBack = false

                    false
                }
            }
        }).attachToRecyclerView(recyclerView)

        //ON CLICK
        additionButton.setOnClickListener {
            val action = MissedAttendanceListFragmentDirections.actionListToDetail(viewModel.getMissedAttendances().size,
                date = getString(R.string.default_date_text), addItem = true)
            findNavController().navigate(action)
        }
    }
}
